<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEntradasalidaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('entradasalida', function (Blueprint $table) {
            $table->increments('id');

            $table->dateTime('hora_entrada');
            $table->dateTime('hora_salida');
            $table->string('detalle');




            $table->integer('id_persona');
            $table->foreign('id_persona')->references('id')-> on('persona')-> onDelete('cascade')-> onUpdate('cascade');
            $table->timestamps();



        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('entradasalida');
    }
}
