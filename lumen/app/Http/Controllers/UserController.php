<?php


namespace App\Http\Controllers;

use App\Http\Models\Prueba;
use Illuminate\Http\Request;
use Exception;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpFoundation\Response;

class UserController
{
    public function  __construct()
    {
    }

    public  function index()
    {
        try{
            $listado = Prueba::all();
            return response()->json($listado,Response::HTTP_OK);
        }catch (Exception $ex){
            return Response()->json(["error" => "hubo un error al listar los usuarios". $ex -> getMessage()],206);
        }

    }

    public function  show(Request $request,$id)
    {
        try{
            $users = DB::find($id);
            return response()->json($users,Response::HTTP_OK);
        }catch (Exception $ex){
            return Response()->json(["error" => "hubo un error al encontrar el cargo con usuario" . $id .":". $ex-> getMessage()],404);
        }

    }

    public function  crear(Request $request)
    {
        try{
            $users = Prueba::create($request -> all());
            return response()->json($users,Response::HTTP_CREATED);
        }catch (Exception $ex){
            return Response()->json(["error" => "hubo un error al registrar un usuario". $ex-> getMessage()],400);
        }

    }

    public function  update(Request $request,$id)
    {
        try{
            $users = Prueba::findOrfail($id);
            $users -> update($request->all());
            return response()->json($users,Response::HTTP_OK);
        }catch (Exception $ex){
            return Response()->json(["error" => "hubo un error al actualizar un usuario".  $id .":". $ex-> getMessage()],400);
        }

    }

    public function  eliminar(Request $request,$id)
    {
        try{
            Prueba::find($id) -> delete();
            return response()->json([],Response::HTTP_OK);
        }catch (Exception $ex){
            return Response()->json(["error" => "hubo un error a eliminar el usua con id".  $id .":". $ex-> getMessage()],400);
        }

    }





}
